/**
 * @file
 * Attaches behavior for the "Cloudflare IP Country Modal" module.
 */

(($, Drupal) => {
  /**
   * Modal for redirecting user.
   *
   * @param {Object} modalSettings settings to configure Drupal.dialog.
   * @param {String} modalSettings.modalMessage the message displayed in body of dialog.
   * @param {String} modalSettings.modalTitle the copy displayed in title of dialog.
   * @param {String} modalSettings.modalCta the copy included in the confirmation button.
   * @param {String} modalSettings.destinationUrl the url to redirect on click.
   * @return {Drupal.dialog} the dialog.
   * @example
   *
   *      buildRedirectModal({
   *        modalTitle: 'Title',
   *        modalMessage: 'Message',
   *        modalCta: 'Continue',
   *        destinationUrl: 'https://www.drupal.org/nerdery'
   *      });
   */
  const buildRedirectModal = modalSettings => {
    const dialog = Drupal.dialog(`<div>${modalSettings.modalMessage}</div>`, {
      title: modalSettings.modalTitle,
      dialogClass: "cf-ip-redirect-dialog",
      resizable: false,
      buttons: [
        {
          text: modalSettings.modalCta,
          class: "button button--primary",
          click() {
            // redirect on click.
            window.location.href = modalSettings.destinationUrl;
          }
        }
      ],
      close(event) {
        // visually hide the DOM element that was used for the dialog.
        jQuery(event.target).addClass("visually-hidden");
      }
    });

    return dialog;
  };

  /**
   * Inits modal if country redirect exists for active path.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.cfIpCountryModal = {
    attach(context, settings) {
      const init = $(window, context).once("cf_ipcountry_modal_init");
      if (init.length) {
        // request cloudflare language code.
        const $request = $.ajax({
          url: "/api/v1/cf-ipcountry-modal/get-country",
          type: "GET"
        });

        // handle request success.
        $request.done(response => {
          const countryCode = response.country_code;

          // exit on empty country code.
          if (!countryCode) {
            return;
          }

          const match = Object.keys(settings.cfIpCountryRedirect).filter(
            key => key === countryCode
          )[0];

          // exit on no match.
          if (!match) {
            return;
          }

          const redirectDialog = buildRedirectModal(
            settings.cfIpCountryRedirect[match]
          );
          redirectDialog.show();
        });

        // handle request failure.
        $request.fail((jqXHR, textStatus, errorThrown) => {
          if (window.console !== undefined) {
            throw new Error(
              `Ajax request failed. Status: ${textStatus}. Description: ${errorThrown}`
            );
          }
        });
      }
    }
  };
})(jQuery, Drupal);
