<?php

namespace Drupal\cf_ipcountry_modal;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of Cloudflare IPCountry Modal entities.
 */
class CfIpCountryModalListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Modal redirect');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['weight'] = $this->t('Modal redirect weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->getStatus() ? $this->t('Enabled') : $this->t('Disabled');
    $row['weight'] = $entity->getWeight();



    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $add_url =   Url::fromRoute('entity.cf_ipcountry_modal.add_form', [], []);

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet. Click <a href="@here">here</a> to create one.', ['@label' => $this->entityType->getPluralLabel(), '@here' => $add_url->toString()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

}
