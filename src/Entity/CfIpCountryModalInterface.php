<?php

namespace Drupal\cf_ipcountry_modal\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cloudflare IPCountry Modal entities.
 */
interface CfIpCountryModalInterface extends ConfigEntityInterface {

  public function getStatus();
  public function setStatus($status);

  public function getWeight();
  public function setWeight($weight);

  public function getCountry();
  public function setCountry($country);

  public function getDestinationUrl();
  public function setDestinationUrl($url);

  public function getModalTitle();
  public function setModalTitle($title);

  public function getModalMessage();
  public function setModalMessage($message);

  public function getModalCta();
  public function setModalCta($cta);

  public function getPathCondition();
  public function setPathCondition($condition);
}
