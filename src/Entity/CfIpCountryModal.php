<?php

namespace Drupal\cf_ipcountry_modal\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Cloudflare IPCountry Modal entity.
 *
 * @ConfigEntityType(
 *   id = "cf_ipcountry_modal",
 *   label = @Translation("Cloudflare IPCountry Modal"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cf_ipcountry_modal\CfIpCountryModalListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cf_ipcountry_modal\Form\CfIpCountryModalForm",
 *       "edit" = "Drupal\cf_ipcountry_modal\Form\CfIpCountryModalForm",
 *       "delete" = "Drupal\cf_ipcountry_modal\Form\CfIpCountryModalDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cf_ipcountry_modal\CfIpCountryModalHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cf_ipcountry_modal",
 *   admin_permission = "administer cloudflare ipcountry modal",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "weight",
 *     "country",
 *     "pathCondition",
 *     "destinationUrl",
 *     "modalTitle",
 *     "modalMessage",
 *     "modalCta",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/cf_ipcountry_modal/{cf_ipcountry_modal}",
 *     "add-form" = "/admin/config/system/cf_ipcountry_modal/add",
 *     "edit-form" = "/admin/config/system/cf_ipcountry_modal/{cf_ipcountry_modal}/edit",
 *     "delete-form" = "/admin/config/system/cf_ipcountry_modal/{cf_ipcountry_modal}/delete",
 *     "collection" = "/admin/config/system/cf_ipcountry_modal"
 *   }
 * )
 */
class CfIpCountryModal extends ConfigEntityBase implements CfIpCountryModalInterface {

  /**
   * The Modal redirect ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Modal redirect label.
   *
   * @var string
   */
  protected $label;

  /**
   * The modal redirect status.
   *
   * @var boolean
   */
  protected $status = FALSE;

  /**
   * The modal redirect weight.
   *
   * @var int
   */
  protected $weight;


  /**
   * The Cloudflare IP Country inISO 3166-1 Alpha 2 country code.
   *
   * @var string
   */
  protected $country;

  /**
   * The Modal redirect path condition configuration.
   *
   * @var array
   */
  protected $pathCondition = [];

  /**
   * The redirect destination URL.
   *
   * @var string
   */
  protected $destinationUrl;

  /**
   * The redirect modal dialog title.
   *
   * @var string
   */
  protected $modalTitle;

  /**
   * The redirect modal dialog message.
   *
   * @var string
   */
  protected $modalMessage;

  /**
   * The redirect modal dialog call-to-action button text.
   *
   * @var string
   */
  protected $modalCta;

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * {@inheritdoc}
   */
  public function setCountry($country) {
    $this->set('country', $country);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationUrl() {
    return $this->destinationUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function setDestinationUrl($url) {
    $this->set('destinationUrl', $url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModalTitle() {
    return $this->modalTitle;
  }

  /**
   * {@inheritdoc}
   */
  public function setModalTitle($title) {
    $this->set('modalTitle', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModalMessage() {
    return $this->modalMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setModalMessage($message) {
    $this->set('modalMessage', $message);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModalCta() {
    return $this->modalCta;
  }

  /**
   * {@inheritdoc}
   */
  public function setModalCta($cta) {
    $this->set('modalCta', $cta);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPathCondition() {
    return $this->pathCondition;
  }

  /**
   * {@inheritdoc}
   */
  public function setPathCondition($condition) {
    $this->set('pathCondition', $condition);
    return $this;
  }

}
