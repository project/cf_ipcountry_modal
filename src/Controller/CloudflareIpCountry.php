<?php

namespace Drupal\cf_ipcountry_modal\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CloudflareIpCountry.
 *
 * Return the Cloudflare IP Country request header value as a Json response.
 */
class CloudflareIpCountry extends ControllerBase {

  /**
   * GeoLocation.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return geoLocation data.
   */
  public function getCountry() {
    $response = [
      'country_code' => $this->getCfIpCountry(),
    ];

    // Setting Cache-Control Header will prevent Varnish from caching.
    $headers = [
      'Cache-Control' => 'private, must-revalidate, no-store, no-cache',
    ];

    return new JsonResponse($response, 200, $headers);
  }

  /**
   * The country code as set by CloudFlare.
   *
   * @return string
   *   The country code.
   */
  private function getCfIpCountry() {
    if (!isset($_SERVER['HTTP_CF_IPCOUNTRY'])) {
      return '';
    }

    $country = $this->filter($_SERVER['HTTP_CF_IPCOUNTRY']);

    if (empty($country)) {
      return '';
    }

    return $country;
  }

  /**
   * Filter the response against attacks.
   *
   * @param string $value
   *   The value to filter.
   *
   * @return string
   *   Filtered value.
   */
  private function filter($value) {
    return Html::escape(Xss::filter($value));
  }

}
