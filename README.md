CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


 INTRODUCTION
 ------------

 If you manage multiple international Consumer packaged goods (CPG) websites and
 need to direct site visitors to a country-specific website, and you use
 Cloudflare DNS with Cloudflare’s IP Geolocation feature, use this module.

 This module allows authenticated users with permission to create modals with a
 title, message and call-to-action button text and destination URL; and
 configure the modal to appear on specific pages for specific site visitors’
 requests associated with Cloudflare’s CF-IPCountry request header.

 Once configured, site visitors from the configured country will see the modal
 with the configured title, message and a call-to-action button. This allows
 site administrators to prompt site visitors to visit a different website.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/cf_ipcountry_modal

  * To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/cf_ipcountry_modal

REQUIREMENTS
------------

This module only requires Drupal core. It does require the use of Cloudflare IP
Geolocation feature to be enabled. See the Cloudflare documentation for more
information.

RECOMMENDED MODULES
-------------------

No other modules are recommend at this time.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:

   - Go to the path /admin/people/permissions.

   - Grant "Administer Cloudflare IPCountry Modals" permission to desired user
     roles.

* Create and configure Cloudflare IPCountry Modals

   - Go to the path /admin/config/system/cf_ipcountry_modal.

   - Follow on-screen instructions to create and configure one or more modals.

TROUBLESHOOTING
---------------

TBD - No info at this time.

Have a problem or a question, create a Drupal.org issue in the queue,
https://www.drupal.org/node/add/project-issue/cf_ipcountry_modal


FAQ
---
There are no FAQs at this time.

MAINTAINERS
-----------

Current maintainers:
 * Michael Miles (mikemiles86) - https://www.drupal.org/u/mikemiles86
 * Jason Want (jasonawant) - https://www.drupal.org/u/jasonawant
